<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <vieno@hakkerinen.eu> -->
# build gentoo packages

This creates gentoo binary packages.

## thanks

Thanks Solène Rapenne for her [blog post](https://dataswamp.org/~solene/2023-03-04-github-actions-building-gentoo-packages.html) to use CI to build gentoo packages.

## License

All files have a proper [REUSE](https://reuse.software/) license & copyright header.

